class ApplicationController < ActionController::Base
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
  end

  def create
    @article = Article.new(article_params)

    @article.save
    redirect_to @article
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy


    redirect_to articles_path
  end

  def delete_all
    @article = Article.delete_all
    redirect_to articles_path
  end


  private
  def article_params
    params.require(:article).permit(:title, :text, :date, :summa,
                                    :predoplata, :ostatok, :cena_yst, :zamer,
                                    :postavshik, :profile, :comments, :number, :cena_postavhika, :m2,
                                    :cena_konstrukciu, :oplata, :dopu, :oplataOST, :oplataPR, :polPR,
                                    :polOST, :polSUM, :dop0, :color,
                                    :razm0,
                                    :kol0,

                                    :dop1,
                                    :razm1,
                                    :kol1,

                                    :dop2,
                                    :razm2,
                                    :kol2,

                                    :dop3,
                                    :razm3,
                                    :kol3,

                                    :dop4,
                                    :razm4,
                                    :kol4,

                                    :dop5,
                                    :razm5,
                                    :kol5,

                                    :dop6,
                                    :razm6,
                                    :kol6,

                                    :dop7,
                                    :razm7,
                                    :kol7,

                                    :dop8,
                                    :razm8,
                                    :kol8,

                                    :dop9,
                                    :razm9,
                                    :kol9,

                                    :dop10,
                                    :razm10,
                                    :kol10,

                                    :dop11,
                                    :razm11,
                                    :kol11,

                                    :dop12,
                                    :razm12,
                                    :kol12,

                                    :dop13,
                                    :razm13,
                                    :kol13,

                                    :dop14,
                                    :razm14,
                                    :kol14,

                                    :dop15,
                                    :razm15,
                                    :kol15,

                                    :dop16,
                                    :razm16,
                                    :kol16,

                                    :dop17,
                                    :razm17,
                                    :kol17,

                                    :dop18,
                                    :razm18,
                                    :kol18,

                                    :dop19,
                                    :razm19,
                                    :kol19,

                                    :dop20,
                                    :razm20,
                                    :kol20,
                                    :color1,

                                    :color2,

                                    :color3,

                                    :color4,

                                    :color5,

                                    :color6,

                                    :color7,

                                    :color8,

                                    :color9,

                                    :color10,

                                    :color11,

                                    :color12,

                                    :color13,

                                    :color14,

                                    :color15,

                                    :color16,

                                    :color17,

                                    :color18,

                                    :color19,

                                    :color20
    )
  end
end
